package org.darkstorm.minecraft.gui.theme.lithium;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import org.darkstorm.minecraft.gui.component.Button;
import org.darkstorm.minecraft.gui.component.Component;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;
import org.lwjgl.input.Mouse;

public class LithiumButtonUI extends AbstractComponentUI<Button> {
	private final LithiumTheme theme;

	LithiumButtonUI(final LithiumTheme theme) {
		super(Button.class);
		this.theme = theme;

		this.foreground = Color.WHITE;
		this.background = new Color(128, 128, 128, 128 + 128 / 2);
	}

	@Override
	protected Dimension getDefaultComponentSize(final Button component) {
		return new Dimension(this.theme.getFontRenderer().getStringWidth(
				component.getText()) + 4, 15);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(final Button component) {
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				component.getHeight()) };
	}

	@Override
	protected void handleComponentInteraction(final Button component,
			final Point location, final int button) {
		if (location.x <= component.getWidth()
				&& location.y <= component.getHeight() && button == 0) {
			component.press();
		}
	}

	@Override
	protected void renderComponent(final Button button) {
		this.translateComponent(button, false);
		final Rectangle area = button.getArea();
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);

		glDisable(GL_TEXTURE_2D);
		RenderUtil.setColor(button.getBackgroundColor());
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 0);
			glVertex2d(area.width, 0);
			glVertex2d(area.width, area.height);
			glVertex2d(0, area.height);
		}
		glEnd();
		final Point mouse = RenderUtil.calculateMouseLocation();
		Component parent = button.getParent();
		while (parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}
		if (area.contains(mouse)) {
			glColor4f(0.0f, 0.0f, 0.0f, Mouse.isButtonDown(0) ? 0.5f : 0.3f);
			glBegin(GL_QUADS);
			{
				glVertex2d(0, 0);
				glVertex2d(area.width, 0);
				glVertex2d(area.width, area.height);
				glVertex2d(0, area.height);
			}
			glEnd();
		}
		glEnable(GL_TEXTURE_2D);

		final String text = button.getText();
		this.theme.getFontRenderer()
				.drawString(
						text,
						area.width
								/ 2
								- this.theme.getFontRenderer().getStringWidth(
										text) / 2,
						area.height / 2
								- this.theme.getFontRenderer().FONT_HEIGHT / 2,
						RenderUtil.toRGBA(button.getForegroundColor()));

		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		this.translateComponent(button, true);
	}
}