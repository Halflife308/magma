package org.darkstorm.minecraft.gui.theme.lithium;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.ScaledResolution;

import org.darkstorm.minecraft.gui.component.Component;
import org.darkstorm.minecraft.gui.component.Slider;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class LithiumSliderUI extends AbstractComponentUI<Slider> {
	private final LithiumTheme theme;

	LithiumSliderUI(final LithiumTheme theme) {
		super(Slider.class);
		this.theme = theme;

		this.foreground = Color.WHITE;
		this.background = new Color(128, 128, 128, 0);
	}

	@Override
	protected Dimension getDefaultComponentSize(final Slider component) {
		return new Dimension(this.theme.getFontRenderer().getStringWidth(
				component.getText()) + 20, 16);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(final Slider component) {
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				component.getHeight()) };
	}

	@Override
	protected void handleComponentInteraction(final Slider component,
			final Point location, final int button) {
		if (button == 0 && component.didAreaContain()) {
			component.setSelected(true);
		}
	}

	@Override
	protected void renderComponent(final Slider slider) {
		this.translateComponent(slider, false);

		slider.setWidth(slider.getParent().getWidth() - 4);
		final Rectangle area = slider.getArea();

		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);

		glDisable(GL_TEXTURE_2D);

		final Point mouse = Iridium.instance.moduleManager.scaleGUI.getToggled() ? RenderUtil.calculateStdMouseLocation() : RenderUtil.calculateMouseLocation();
		Component parent = slider.getParent();

		while (parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}

		glColor4f(0.266f, 0.266f, 0.266f, area.contains(mouse) ? 0.8f : 0.9f);
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 1);
			glVertex2d(area.width, 1);
			glVertex2d(area.width, area.height);
			glVertex2d(0, area.height);
		}
		GL11.glEnd();

		glLineWidth(1.0f);
		glColor4f(0.24f, 0.24f, 0.24f, 1.0f);

		final double sliderWidth = area.width
				* ((slider.getValue() - slider.getMinValue()) / (slider
						.getMaxValue() - slider.getMinValue()));

		glLineWidth(1.0f);
		glColor4f(0.0f, 1f, 0.4f, 0.7f);

		for (final Component component : slider.getParent().getChildren()) {
			if (component instanceof Slider) {
				final Slider compSlider = (Slider) component;
				if (compSlider.getSelected() && compSlider != slider) {
				}
			}
		}

		slider.setAreaContained(area.contains(mouse));

		// if ((area.contains(mouse) && !isOtherSliderSelected)
		// || slider.getSelected()) {
		// hasMouse = true;
		// glColor4f(1f, 1f, 1f, slider.getSelected()
		// && !isOtherSliderSelected ? 0.15f : 0.11f);
		// glBegin(GL_QUADS);
		// {
		// glVertex2d(0, 0);
		// glVertex2d(sliderWidth, 0);
		// glVertex2d(sliderWidth, area.height - 1);
		// glVertex2d(0, area.height - 1);
		// }
		// glEnd();
		// }
		glEnable(GL_TEXTURE_2D);

		if (!Mouse.isButtonDown(0)) {
			slider.setSelected(false);
		}

		if (slider.getSelected()) {
			final int scaledWidth = new ScaledResolution(
					Minecraft.getMinecraft(),
					Minecraft.getMinecraft().displayWidth,
					Minecraft.getMinecraft().displayHeight).getScaledWidth();
			final int mouseX = Mouse.getX() * scaledWidth
					/ Minecraft.getMinecraft().displayWidth;

			final Point location = new Point(mouseX - 2
					- slider.getParent().getX(), Mouse.getY());

			final double width = area.getMaxX() - area.getMinX();

			final double percent = location.x / width;

			final float value = (float) (slider.getMinValue() + percent
					* (slider.getMaxValue() - slider.getMinValue()));

			slider.setValue(value);

			slider.onValueChanged();
		}

		RenderUtil.drawGradientRect(0,
				1, sliderWidth, area.height, 0xbf1b7bb8, 0xbf176ba0);

		glEnable(GL_TEXTURE_2D);

		final String text = slider.getText() + " - "
				+ String.valueOf(slider.getValue()).replace(".0", "");
		this.theme.getFontRenderer().drawStringWithShadow(
				text,
				area.width / 2
						- this.theme.getFontRenderer().getStringWidth(text) / 2
						+ 0.5f,
				area.height / 2 - this.theme.getFontRenderer().FONT_HEIGHT / 2
						+ 0.5f, 0x8f000000);
		this.theme.getFontRenderer()
				.drawString(
						text,
						area.width
								/ 2
								- this.theme.getFontRenderer().getStringWidth(
										text) / 2,
						area.height / 2
								- this.theme.getFontRenderer().FONT_HEIGHT / 2,
						0xffffffff);

		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		this.translateComponent(slider, true);

		slider.setMouseDown(Mouse.isButtonDown(0));
	}
}