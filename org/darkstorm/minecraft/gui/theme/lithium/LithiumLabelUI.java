package org.darkstorm.minecraft.gui.theme.lithium;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;

import java.awt.Color;
import java.awt.Dimension;

import org.darkstorm.minecraft.gui.component.Label;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;

public class LithiumLabelUI extends AbstractComponentUI<Label> {
	private final LithiumTheme theme;

	LithiumLabelUI(final LithiumTheme theme) {
		super(Label.class);
		this.theme = theme;

		this.foreground = Color.WHITE;
		this.background = new Color(128, 128, 128, 128);
	}

	@Override
	protected Dimension getDefaultComponentSize(final Label component) {
		return new Dimension(this.theme.getFontRenderer().getStringWidth(
				component.getText()) + 4,
				this.theme.getFontRenderer().FONT_HEIGHT + 4);
	}

	@Override
	protected void renderComponent(final Label label) {
		this.translateComponent(label, false);
		int x = 0, y = 0;
		switch (label.getHorizontalAlignment()) {
		case CENTER:
			x += label.getWidth()
					/ 2
					- this.theme.getFontRenderer().getStringWidth(
							label.getText()) / 2;
			break;
		case RIGHT:
			x += label.getWidth()
					- this.theme.getFontRenderer().getStringWidth(
							label.getText()) - 2;
			break;
		default:
			x += 2;
		}
		switch (label.getVerticalAlignment()) {
		case TOP:
			y += 2;
			break;
		case BOTTOM:
			y += label.getHeight() - this.theme.getFontRenderer().FONT_HEIGHT
					- 2;
			break;
		default:
			y += label.getHeight() / 2
					- this.theme.getFontRenderer().FONT_HEIGHT / 2;
		}
		glEnable(GL_BLEND);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_CULL_FACE);
		this.theme.getFontRenderer().drawString(label.getText(), x, y,
				RenderUtil.toRGBA(label.getForegroundColor()));
		glEnable(GL_CULL_FACE);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_BLEND);
		this.translateComponent(label, true);
	}
}