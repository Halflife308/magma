package org.darkstorm.minecraft.gui.theme.lithium;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import org.darkstorm.minecraft.gui.component.CheckButton;
import org.darkstorm.minecraft.gui.component.Component;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

public class LithiumCheckButtonUI extends AbstractComponentUI<CheckButton> {
	private final LithiumTheme theme;

	LithiumCheckButtonUI(final LithiumTheme theme) {
		super(CheckButton.class);
		this.theme = theme;

		this.foreground = Color.WHITE;
		this.background = new Color(128, 128, 128, 128);
	}

	@Override
	protected Dimension getDefaultComponentSize(final CheckButton component) {
		return new Dimension(this.theme.getFontRenderer().getStringWidth(
				component.getText()) + 12, 15);
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(
			final CheckButton component) {
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				component.getHeight()) };
	}

	@Override
	protected void handleComponentInteraction(final CheckButton component,
			final Point location, final int button) {
		if (location.x <= component.getWidth()
				&& location.y <= component.getHeight() && button == 0) {
			component.press();
		}
	}

	@Override
	protected void renderComponent(final CheckButton button) {
		this.translateComponent(button, false);
		final Rectangle area = button.getArea();
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);

		final int parentWidth = button.getParent().getWidth() - 4;
		button.setWidth(parentWidth);

		final Point mouse = RenderUtil.calculateMouseLocation();
		Component parent = button.getParent();
		while (parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}

		if (area.contains(mouse))
		{
			RenderUtil.drawBorderedRect(0,
					1, parentWidth, area.height, 0.5F, button.isSelected() ? 0xbf1b7bb
							: 0x8f6f6b6b, button.isSelected() ? 0xbf176ba0
							: 0xaf535151);
		} else {
			RenderUtil.drawGradientRect(0,
				1, parentWidth, area.height, button.isSelected() ? 0xbf1b7bb8
						: 0x8f6f6b6b, button.isSelected() ? 0xbf176ba0
						: 0xaf535151);
		}

		glDisable(GL11.GL_TEXTURE_2D);
		glDisable(GL_CULL_FACE);
		glEnable(GL_BLEND);
		glColor4f(0.2f / 6,
				0.2f / 6,
				0.2f / 6, 0);
		glBegin(GL_QUADS);
		{
			glVertex2d(0, 1);
			glVertex2d(parentWidth, 1);
			glVertex2d(parentWidth, area.height);
			glVertex2d(0, area.height);
		}
		glEnd();

		GL11.glDisable(GL11.GL_TEXTURE_2D);

		GL11.glEnable(GL11.GL_BLEND);

		glEnable(GL_TEXTURE_2D);

		final String text = button.getText();
		this.theme.getFontRenderer().func_175063_a(
				text,
				3.5f,
				area.height / 2
						- this.theme.getFontRenderer().getStringHeight(text)
						/ 2 + 0.5f, 0x8f000000);
		this.theme.getFontRenderer().drawString(
				text,
				3,
				area.height / 2
						- this.theme.getFontRenderer().getStringHeight(text)
						/ 2, button.isSelected() ? 0xffffffff : 0xffffffff);

		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		this.translateComponent(button, true);
	}
}