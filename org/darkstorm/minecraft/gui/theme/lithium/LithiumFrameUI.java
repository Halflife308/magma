package org.darkstorm.minecraft.gui.theme.lithium;

import static org.lwjgl.opengl.GL11.GL_BLEND;
import static org.lwjgl.opengl.GL11.GL_CULL_FACE;
import static org.lwjgl.opengl.GL11.GL_ONE_MINUS_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.GL_SRC_ALPHA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glVertex2d;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;

import org.darkstorm.minecraft.gui.component.Component;
import org.darkstorm.minecraft.gui.component.Frame;
import org.darkstorm.minecraft.gui.layout.Constraint;
import org.darkstorm.minecraft.gui.theme.AbstractComponentUI;
import org.darkstorm.minecraft.gui.util.RenderUtil;
import org.lwjgl.opengl.GL11;

public class LithiumFrameUI extends AbstractComponentUI<Frame> {
	private final LithiumTheme theme;

	LithiumFrameUI(final LithiumTheme theme) {
		super(Frame.class);
		this.theme = theme;

		this.foreground = Color.WHITE;
		this.background = new Color(0, 0, 0, 255);
	}

	@Override
	protected Rectangle getContainerChildRenderArea(final Frame container) {
		final Rectangle area = new Rectangle(container.getArea());
		area.x = 2;
		area.y = this.theme.getFontRenderer().FONT_HEIGHT + 6;
		area.width -= 4;
		area.height -= this.theme.getFontRenderer().FONT_HEIGHT + 8;
		return area;
	}

	@Override
	protected Dimension getDefaultComponentSize(final Frame component) {
		final Component[] children = component.getChildren();
		final Rectangle[] areas = new Rectangle[children.length];
		final Constraint[][] constraints = new Constraint[children.length][];
		for (int i = 0; i < children.length; i++) {
			final Component child = children[i];
			final Dimension size = child.getTheme().getUIForComponent(child)
					.getDefaultSize(child);
			areas[i] = new Rectangle(0, 0, size.width, size.height);
			constraints[i] = component.getConstraints(child);
		}
		final Dimension size = component.getLayoutManager()
				.getOptimalPositionedSize(areas, constraints);
		size.width += 4;
		size.height += this.theme.getFontRenderer().FONT_HEIGHT + 8;
		return size;
	}

	@Override
	protected Rectangle[] getInteractableComponentRegions(final Frame component) {
		return new Rectangle[] { new Rectangle(0, 0, component.getWidth(),
				this.theme.getFontRenderer().FONT_HEIGHT + 4) };
	}

	@Override
	protected void handleComponentInteraction(final Frame component,
			final Point location, final int button) {
		if (button != 0) {
			return;
		}
		int offset = component.getWidth() - 2;
		final int textHeight = this.theme.getFontRenderer().FONT_HEIGHT;
		if (component.isClosable()) {
			if (location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.close();
				return;
			}
			offset -= textHeight + 2;
		}
		if (component.isPinnable()) {
			if (location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.setPinned(!component.isPinned());
				return;
			}
			offset -= textHeight + 2;
		}
		if (component.isMinimizable()) {
			if (location.x >= offset - textHeight && location.x <= offset
					&& location.y >= 2 && location.y <= textHeight + 2) {
				component.setMinimized(!component.isMinimized());
				return;
			}
			offset -= textHeight + 2;
		}
		if (location.x >= 0 && location.x <= offset && location.y >= 0
				&& location.y <= textHeight + 4) {
			component.setDragging(true);
			return;
		}
	}

	@Override
	protected void renderComponent(final Frame component) {
		final Rectangle area = new Rectangle(component.getArea());
		final int fontHeight = this.theme.getFontRenderer().FONT_HEIGHT;
		this.translateComponent(component, false);
		glEnable(GL_BLEND);
		glDisable(GL_CULL_FACE);
		glDisable(GL_TEXTURE_2D);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// Draw frame background
		if (component.isMinimized()) {
			area.height = fontHeight + 4;
		}

		glColor4f(148 / 255f, 145 / 255f, 139 / 255f, 0.9f);
		glBegin(GL_QUADS);
		{
			glVertex2d(-1, 0);
			glVertex2d(area.width + 1, 0);
			glVertex2d(area.width + 1, fontHeight + 4);
			glVertex2d(-1, fontHeight + 4);
		}
		glEnd();

		if (!component.isMinimized()) {
			glColor4f(0f, 0f, 0f, 0.3f);
			glBegin(GL_QUADS);
			{
				glVertex2d(0, fontHeight + 4);
				glVertex2d(area.width, fontHeight + 4);
				glVertex2d(area.width, area.height);
				glVertex2d(0, area.height);
			}
			glEnd();
		}

		glLineWidth(0.5f);
		glColor4f(0.5f, 0.5f, 0.5f, 0.5f);

		// Draw controls
		int offset = component.getWidth() - 2;
		final Point mouse = RenderUtil.calculateMouseLocation();
		Component parent = component;
		while (parent != null) {
			mouse.x -= parent.getX();
			mouse.y -= parent.getY();
			parent = parent.getParent();
		}
		final boolean[] checks = new boolean[] { component.isClosable(),
				component.isPinnable(), component.isMinimizable() };
		final boolean[] overlays = new boolean[] { false,
				!component.isPinned(), component.isMinimized() };
		for (int i = 0; i < checks.length; i++) {
			if (!checks[i]) {
				continue;
			}
			RenderUtil.setColor(component.getBackgroundColor());
			glBegin(GL_QUADS);
			{
				glVertex2d(offset - fontHeight, 2);
				glVertex2d(offset, 2);
				glVertex2d(offset, fontHeight + 2);
				glVertex2d(offset - fontHeight, fontHeight + 2);
			}
			glEnd();

			if (overlays[i]) {
				glColor4f(0.0f, 0.0f, 0.0f, 0.5f);
				glBegin(GL_QUADS);
				{
					glVertex2d(offset - fontHeight, 2);
					glVertex2d(offset, 2);
					glVertex2d(offset, fontHeight + 2);
					glVertex2d(offset - fontHeight, fontHeight + 2);
				}
				glEnd();
			}
			if (mouse.x >= offset - fontHeight && mouse.x <= offset
					&& mouse.y >= 2 && mouse.y <= fontHeight + 2) {
				glColor4f(0.0f, 0.0f, 0.0f, 0.3f);
				glBegin(GL_QUADS);
				{
					glVertex2d(offset - fontHeight, 2);
					glVertex2d(offset, 2);
					glVertex2d(offset, fontHeight + 2);
					glVertex2d(offset - fontHeight, fontHeight + 2);
				}
				glEnd();
			}
			glLineWidth(0.5f);
			glColor4f(!overlays[i] ? 0.12156862745f : 0.25490196078f,
					!overlays[i] ? 0.43137254902f : 0.25490196078f,
					!overlays[i] ? 0.62352941176f : 0.25490196078f, 0.9f);
			glBegin(GL11.GL_QUADS);
			{
				glVertex2d(offset - fontHeight, 2);
				glVertex2d(offset, 2);
				glVertex2d(offset, fontHeight + 2);
				glVertex2d(offset - fontHeight, fontHeight + 2);
			}
			glEnd();
			offset -= fontHeight + 2;
		}

		glEnable(GL_TEXTURE_2D);
		this.theme.titleFontRenderer.func_175063_a(component.getTitle()
				.toUpperCase(), 2.7f, 2.7f, 0xaf000000);
		this.theme.titleFontRenderer.func_175063_a(component.getTitle()
				.toUpperCase(), 2, 2, RenderUtil.toRGBA(component
				.getForegroundColor()));
		glEnable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		this.translateComponent(component, true);
	}
}
