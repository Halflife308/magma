package org.darkstorm.minecraft.gui.theme.lithium;

import java.awt.Font;

import org.darkstorm.minecraft.gui.font.UnicodeFontRenderer;
import org.darkstorm.minecraft.gui.theme.AbstractTheme;

public class LithiumTheme extends AbstractTheme {
	private final UnicodeFontRenderer fontRenderer;
	public final UnicodeFontRenderer titleFontRenderer;

	// Button on - 31, 110, 159
	// Button off - 65, 65, 65
	// Font off - 959595
	// Font on - FFFFFF

	public LithiumTheme() {

		this.fontRenderer = new UnicodeFontRenderer(
				new Font("Verdana", Font.TRUETYPE_FONT, 16));
		this.titleFontRenderer = new UnicodeFontRenderer(
				new Font("Verdana", Font.TRUETYPE_FONT, 17));

		this.installUI(new LithiumFrameUI(this));
		this.installUI(new LithiumPanelUI(this));
		this.installUI(new LithiumLabelUI(this));
		this.installUI(new LithiumButtonUI(this));
		this.installUI(new LithiumCheckButtonUI(this));
		this.installUI(new LithiumComboBoxUI(this));
		//this.installUI(new LithiumSliderUI(this));
	}

	public UnicodeFontRenderer getFontRenderer() {
		return this.fontRenderer;
	}
}
