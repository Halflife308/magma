package me.jamie.magma.command;

public interface ICommand 
{
	abstract void onCommand(String[] args);
}
