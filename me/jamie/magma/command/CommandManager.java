package me.jamie.magma.command;

import java.util.LinkedList;
import java.util.List;

public class CommandManager 
{
	private List<Command> commands = new LinkedList<Command>();
	private String prefix = ".";
	
	public List<Command> getCommands() {
		return commands;
	}
	
	public final void loadCommands() {
		
	}
	
	public String getPrefix() {
		return prefix;
	}
	
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
