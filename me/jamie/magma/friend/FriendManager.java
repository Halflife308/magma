package me.jamie.magma.friend;

import java.util.LinkedList;
import java.util.List;

public class FriendManager 
{
	private List<Friend> friends = new LinkedList<Friend>();
	
	public List<Friend> getFriends() {
		return friends;
	}
	
	public void addFriend(Friend f)
	{
		if(!friends.contains(f))
		{
			friends.add(f);
		}
	}
	
	public String getUsername(String s) 
	{
		for(Friend f : friends)
		{
			if(f.getName().equalsIgnoreCase(s))
			{
				return s;
			}
		}
		return s;
	}
	
	public void removeFriend(Friend f)
	{
		if(friends.contains(f))
		{
			friends.remove(f);
		}
	}
	
	public boolean isFriend(String name)
	{
		for(Friend f : friends)
		{
			if(name.equalsIgnoreCase(f.getName()))
			{
				return true;
			}
		}
		return false;
	}
}
