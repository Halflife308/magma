package me.jamie.magma.friend;

public class Friend 
{
	protected String name, alias;
	
	public Friend(String name, String alias)
	{
		this.name = name;
		this.alias = alias;
	}
	
	public String getName() {
		return name;
	}
	
	public String getAlias() {
		return alias;
	}
}

