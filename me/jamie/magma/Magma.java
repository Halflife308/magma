package me.jamie.magma;

import java.io.IOException;

import me.jamie.magma.command.CommandManager;
import me.jamie.magma.file.FileManager;
import me.jamie.magma.friend.FriendManager;
import me.jamie.magma.gui.GuiManager;
import me.jamie.magma.gui.MagmaUI;
import me.jamie.magma.mod.ModuleManager;
import me.jamie.magma.util.Utils;
import me.jamie.magma.util.Wrapper;

import org.darkstorm.minecraft.gui.theme.lithium.LithiumTheme;

public final class Magma 
{
	private static final Magma INSTANCE = new Magma();
	
	private final CommandManager COMMAND_MANAGER = new CommandManager();
	private final FileManager FILE_MANAGER = new FileManager();
	private final FriendManager FRIEND_MANAGER = new FriendManager();
	private final GuiManager GUI_MANAGER = new GuiManager();
	private final ModuleManager MODULE_MANAGER = new ModuleManager();
	private final Utils UTILS = new Utils();
	
	public static final Magma getMagma() {
		return INSTANCE;
	}
	
	public final void start() 
	{
		UTILS.addConsoleOutput("Loading Magma...");
		MODULE_MANAGER.loadModules();
		UTILS.addConsoleOutput("Loaded " + MODULE_MANAGER.getModules().size() + " modules.");
		COMMAND_MANAGER.loadCommands();
		UTILS.addConsoleOutput("Loaded " + COMMAND_MANAGER.getCommands().size() + " commands.");
		GUI_MANAGER.setTheme(new LithiumTheme());
		GUI_MANAGER.setup();
		try {
			FILE_MANAGER.loadFiles();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Wrapper.getMinecraft().ingameGUI = new MagmaUI(Wrapper.getMinecraft());
		UTILS.addConsoleOutput("Finished loading Magma.");
	}
	
	public final CommandManager getCommandManager() {
		return COMMAND_MANAGER;
	}
	
	public final FileManager getFileManager() {
		return FILE_MANAGER;
	}
	
	public final FriendManager getFriendManager() {
		return FRIEND_MANAGER;
	}
	
	public final GuiManager getGuiManager() {
		return GUI_MANAGER;
	}
	
	public final ModuleManager getModuleManager() {
		return MODULE_MANAGER;
	}
	
	public final Utils getUtils() {
		return UTILS;
	}
}
