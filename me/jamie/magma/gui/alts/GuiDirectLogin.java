/**
 * @author Jamie
 * @since 19/01/2015
 */
package me.jamie.magma.gui.alts;

import java.io.IOException;
import java.net.Proxy;

import me.jamie.magma.util.Wrapper;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.Session;

import org.lwjgl.input.Keyboard;

import com.mojang.authlib.Agent;
import com.mojang.authlib.exceptions.AuthenticationException;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.authlib.yggdrasil.YggdrasilUserAuthentication;

public class GuiDirectLogin extends GuiScreen
{
	private String progress;
	private GuiTextField userField;
	private GuiPasswordField passwordField;
	
	public void initGui() 
	{
		Keyboard.enableRepeatEvents(true);
        this.buttonList.clear();
        this.buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, "Login"));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, "Back"));
        this.userField = new GuiTextField(2, this.fontRendererObj, this.width / 2 - 100, 76, 200, 20);
        this.userField.setMaxStringLength(128);
        this.userField.setFocused(true);
        this.passwordField = new GuiPasswordField(2, this.fontRendererObj, this.width / 2 - 100, 116, 200, 20);
        this.passwordField.setMaxStringLength(128);
	}
	
	 public void drawScreen(int mouseX, int mouseY, float partialTicks)
	 {
	        this.drawDefaultBackground();
	        this.drawCenteredString(this.fontRendererObj, progress, this.width / 2, 20, 16777215);
	        this.drawString(this.fontRendererObj, "Username:", this.width / 2 - 100, 63, 10526880);
	        this.drawString(this.fontRendererObj, "Password:", this.width / 2 - 100, 104, 10526880);
	        this.userField.drawTextBox();
	        this.passwordField.drawTextBox();
	        super.drawScreen(mouseX, mouseY, partialTicks);
	 }
	
	protected void actionPerformed(GuiButton button) throws IOException
	{
		if(button.id == 1)
		{
			Wrapper.getMinecraft().displayGuiScreen(new GuiMainMenu());
		} else if(button.id == 0)
		{
			if((!userField.getText().isEmpty() && (!passwordField.getText().isEmpty())))
			{
				String name = userField.getText();
				String password = passwordField.getText();
				processLogin(name, password);
			} else {
				progress = "Enter valid credidentials.";
			}
		}
	}
	
    protected void keyTyped(char par1, int par2) throws IOException {
        userField.textboxKeyTyped(par1, par2);
        passwordField.textboxKeyTyped(par1, par2);
        if (par1 == '\t' && (this.userField.isFocused() || this.passwordField.isFocused())) {
            this.userField.setFocused(!this.userField.isFocused());
            this.passwordField.setFocused(!this.passwordField.isFocused());
        }

        if (par1 == '\r') {
            actionPerformed((GuiButton)buttonList.get(0));
        }
    }
	
	protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException
    {
        super.mouseClicked(mouseX, mouseY, mouseButton);
        this.userField.mouseClicked(mouseX, mouseY, mouseButton);
        this.passwordField.mouseClicked(mouseX, mouseY, mouseButton);
    }
	
	public void processLogin(String name, String password)
	{
		try {
			YggdrasilUserAuthentication auth = new YggdrasilUserAuthentication(new YggdrasilAuthenticationService(Proxy.NO_PROXY, ""), new Agent("minecraft", 1));
			auth.setUsername(name);
			auth.setPassword(password);
			try {
				auth.logIn();
				Wrapper.getMinecraft().session = new Session(auth.getSelectedProfile().getName(), auth.getSelectedProfile().getId().toString(), auth.getAuthenticatedToken(), "legacy");
				progress = "�2Login successful.";
			} catch (AuthenticationException ae) {
				progress = "�4Invalid username/password.";
			}
		} catch (Exception e) {
			progress = "�4Error logging in.";
		}
	}
}
