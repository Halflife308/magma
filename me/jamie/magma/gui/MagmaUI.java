package me.jamie.magma.gui;

import java.io.IOException;

import me.jamie.magma.Magma;
import me.jamie.magma.mod.Module;
import me.jamie.magma.util.Wrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;

import org.darkstorm.minecraft.gui.component.Frame;
import org.darkstorm.minecraft.gui.util.GuiManagerDisplayScreen;
import org.lwjgl.input.Keyboard;

public class MagmaUI extends GuiIngame
{

	public MagmaUI(Minecraft mcIn) 
	{
		super(mcIn);
	}
	
	public void func_175180_a(float p_175180_1_)
	{
		super.func_175180_a(p_175180_1_);
		if(Wrapper.getGameSettings().showDebugInfo) return;
		
		Wrapper.getFontRenderer().func_175063_a("Magma", 2, 2, -1);
		
		int y = 2;
		for(Module m : Magma.getMagma().getModuleManager().getModules()) {
			if((m.isEnabled()) && (m.isVisible())) {
				Wrapper.getFontRenderer().func_175063_a(m.getName(), Wrapper.getScaledResolution().getScaledWidth() - Wrapper.getFontRenderer().getStringWidth(m.getName()) - 2, y, m.getColor());
				y += 9;
			}
		}
		
		for(Frame frame : Magma.getMagma().getGuiManager().getFrames()) 
		{
			if(frame.isPinned() || Wrapper.getMinecraft().currentScreen instanceof GuiManagerDisplayScreen) {
				frame.render();
			}
		}
	}
	
}
