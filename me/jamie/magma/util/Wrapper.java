package me.jamie.magma.util;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.settings.GameSettings;

public class Wrapper
{
	private static Minecraft mc = Minecraft.getMinecraft();
	
	public static Minecraft getMinecraft() {
		return mc;
	}
	
	public static EntityPlayerSP getPlayer() {
		return mc.thePlayer;
	}
	
	public static WorldClient getWorld() {
		return mc.theWorld;
	}
	
	public static GameSettings getGameSettings() {
		return mc.gameSettings;
	}
	
	public static FontRenderer getFontRenderer() {
		return mc.fontRendererObj;
	}
	
	public static ScaledResolution getScaledResolution() {
		return new ScaledResolution(mc, mc.displayWidth, mc.displayHeight);
	}
}
