package me.jamie.magma.util;

import net.minecraft.util.ChatComponentText;

public class Utils 
{
	public final static void addChatMessage(String s) {
		if((s == null) || (Wrapper.getWorld() == null)) return;
		Wrapper.getPlayer().addChatMessage(new ChatComponentText("�3[Magma]: " + s));
	}
	
	public final static void addConsoleOutput(String s) {
		if(s == null) return;
		System.out.println("[Magma]: " + s);
	}
}
