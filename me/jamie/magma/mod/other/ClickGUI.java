package me.jamie.magma.mod.other;

import java.io.IOException;

import me.jamie.magma.Magma;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Wrapper;

import org.darkstorm.minecraft.gui.util.GuiManagerDisplayScreen;
import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;

public class ClickGUI extends Module
{

	public ClickGUI() 
	{
		super("ClickGUI", Keyboard.KEY_RSHIFT, 0);
		setCategory(ModuleCategory.Other);
	}

	@Override
	public void onEnable() 
	{
		EventManager.register(this);
		Wrapper.getMinecraft().displayGuiScreen(new GuiManagerDisplayScreen(Magma.getMagma().getGuiManager()));
		try {
			Magma.getMagma().getFileManager().getPositions().saveFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setEnabled(false);
	}

	@Override
	public void onDisable() 
	{
		try {
			Magma.getMagma().getFileManager().getPinned().saveFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		EventManager.unregister(this);
	}
	
}
