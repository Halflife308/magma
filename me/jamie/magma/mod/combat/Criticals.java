package me.jamie.magma.mod.combat;

import java.lang.reflect.Field;

import me.jamie.magma.events.packet.EventPacketSend;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Utils;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

public class Criticals extends Module
{

	public Criticals() 
	{
		super("Criticals", Keyboard.KEY_C, 0xFFCC33);
		setVisible(true);
		setCategory(ModuleCategory.Combat);
	}

	@Override
	public void onEnable() 
	{
		EventManager.register(this);
	}

	@Override
	public void onDisable() 
	{
		EventManager.unregister(this);
	}
	
	@EventTarget
	private void onPacketSend(final EventPacketSend event)
	{
		 Packet packet = event.getPacket();
	        if(packet instanceof C03PacketPlayer) {
	            C03PacketPlayer packetPlayer = (C03PacketPlayer) packet;
	            EntityPlayer player = Minecraft.getMinecraft().thePlayer;
	            boolean onGround = false;
	            
	            if(!packetPlayer.func_149465_i() && player.motionY < 0 && player.motionY > -0.17) {
	            	onGround = true;
	            }
	            
	            Class packetClass = packet.getClass();
	            if(packetClass != C03PacketPlayer.class) {
	                packetClass = packetClass.getSuperclass();
	            }

	            for(Field field : packetClass.getDeclaredFields()) {
	                field.setAccessible(true);
	                if(field.getType() != boolean.class) {
	                    continue;
	                }
	                try {
	                    field.set(packet, onGround);
	                } catch (IllegalAccessException e) {
	                    e.printStackTrace();
	                }
	                return;
	            }
	        }
	}
}
