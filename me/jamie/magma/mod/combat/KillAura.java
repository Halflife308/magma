package me.jamie.magma.mod.combat;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import me.jamie.magma.events.packet.EventPacketSend;
import me.jamie.magma.events.tick.EventUpdate;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Timer;
import me.jamie.magma.util.Wrapper;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityMob;
import net.minecraft.entity.passive.EntityAnimal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

public class KillAura extends Module
{
	private List<EntityLivingBase> targets = new LinkedList<>();
	private Timer timer = new Timer();
	
	private double range = 3.5D;
	private float speed = 3.7F;
	private boolean mobs, animals, players;
	
	public KillAura() 
	{
		super("Kill Aura", Keyboard.KEY_R, 0xFF0000);
		setVisible(true);
		setCategory(ModuleCategory.Combat);
	}

	@Override
	public void onEnable() 
	{
		EventManager.register(this);
	}

	@Override
	public void onDisable() 
	{
		EventManager.unregister(this);
		targets.clear();
	}
	
	@EventTarget
	private void onUpdate(final EventUpdate event)
	{
		if(targets.isEmpty()) {
			populateTargets();
		}
		
		if(timer.check(1000 / speed)) {
			attackTarget();
			timer.reset();
		}
		
		if(!targets.isEmpty()) {
			targets.clear();
		}
	}
	
	@EventTarget
	private void onPacketSend(final EventPacketSend event)
	{
		Packet packet = event.getPacket();
		if(packet instanceof C03PacketPlayer) {
			C03PacketPlayer pp = (C03PacketPlayer)packet;
			if(!targets.isEmpty()) {
				pp.yaw = 95;
			}
		}
	}
	
	private void attackTarget()
	{
		for(int i = 0; i < targets.size(); i++) {
			EntityLivingBase elb = targets.get(i);
			Wrapper.getPlayer().swingItem();
			Wrapper.getMinecraft().playerController.attackEntity(Wrapper.getPlayer(), elb);
		}
	}
	
	private void populateTargets() 
	{
		for(Object o : Wrapper.getWorld().loadedEntityList) 
		{
			if(!(o instanceof EntityPlayerSP) && (o instanceof EntityLivingBase))
			{
				EntityLivingBase elb = (EntityLivingBase)o;
				if(canAttackTarget(elb) && (!targets.contains(elb))) {
					targets.add(elb);
					Collections.sort(targets, new Comparator<EntityLivingBase>() {
						@Override
						public int compare(EntityLivingBase e1, EntityLivingBase e2)
						{
							return Float.compare(e1.getDistanceToEntity(Wrapper.getPlayer()), e2.getDistanceToEntity(Wrapper.getPlayer()));
						}
					});
					if(targets.size() > 5) {
						targets.remove(targets.size() - 1);
					}
				} else {
					targets.remove(elb);
				}
			}
		}
	}
	
	private boolean canAttackTarget(EntityLivingBase elb) 
	{
		return !elb.isDead && Wrapper.getPlayer().getDistanceToEntity(elb) <= range && elb.ticksExisted > 20;
	}
	
	private EntityLivingBase getTarget(EntityLivingBase elb) 
	{
		if((elb instanceof EntityAnimal) && (this.animals)) {
			return elb;
		}
		if((elb instanceof EntityMob) && (this.mobs)) {
			return elb;
		}
		if((elb instanceof EntityPlayer) && (this.players)) {
			return elb;
		}
		return elb;
	}
}
