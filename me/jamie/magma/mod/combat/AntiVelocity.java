package me.jamie.magma.mod.combat;

import me.jamie.magma.events.packet.EventPacketReceive;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Utils;
import net.minecraft.network.play.server.S12PacketEntityVelocity;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

public class AntiVelocity extends Module
{

	public AntiVelocity() 
	{
		super("AntiVelocity", 0, 0xCCCCCC);
		setVisible(true);
		setCategory(ModuleCategory.Combat);
	}

	@Override
	public void onEnable() 
	{
		EventManager.register(this);
	}

	@Override
	public void onDisable() 
	{
		EventManager.unregister(this);
	}
	
	@EventTarget
	private void onPacketReceive(final EventPacketReceive event)
	{
		if(event.getPacket() instanceof S12PacketEntityVelocity) {
			event.setCancelled(true);
		}
	}
}
