package me.jamie.magma.mod;

import java.util.LinkedList;
import java.util.List;

import me.jamie.magma.mod.combat.AntiVelocity;
import me.jamie.magma.mod.combat.Criticals;
import me.jamie.magma.mod.combat.KillAura;
import me.jamie.magma.mod.movement.Jesus;
import me.jamie.magma.mod.movement.Speed;
import me.jamie.magma.mod.movement.Sprint;
import me.jamie.magma.mod.movement.Step;
import me.jamie.magma.mod.other.ClickGUI;

public class ModuleManager
{
	private List<Module> modules = new LinkedList<Module>();
	
	public List<Module> getModules() {
		return modules;
	}
	
	public final void loadModules() {
		this.modules.add(new AntiVelocity());
		this.modules.add(new ClickGUI());
		this.modules.add(new Criticals());
		this.modules.add(new Jesus());
		this.modules.add(new KillAura());
		this.modules.add(new Sprint());
		this.modules.add(new Speed());
		this.modules.add(new Step());
	}
}
