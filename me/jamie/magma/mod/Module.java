package me.jamie.magma.mod;

import com.darkmagician6.eventapi.events.Event;

public abstract class Module implements IModule, Event
{
	protected String name, description;
	protected int bind, color;
	private boolean enabled, visible;
	protected ModuleCategory category;
	
	public Module(String name, int bind, int color) {
		this.name = name;
		this.bind = bind;
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getBind() {
		return bind;
	}

	public void setBind(int bind) {
		this.bind = bind;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public ModuleCategory getCategory() {
		return category;
	}

	public void setCategory(ModuleCategory category) {
		this.category = category;
	}

	public final void onToggle() {
		this.enabled = !this.enabled;
		if(this.enabled) {
			onEnable();
		} else {
			onDisable();
		}
	}
}
