package me.jamie.magma.mod;

public interface IModule 
{
	abstract void onEnable();
	
	abstract void onDisable();
}
