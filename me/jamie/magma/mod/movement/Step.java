package me.jamie.magma.mod.movement;

import me.jamie.magma.events.tick.EventUpdate;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Wrapper;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

public class Step extends Module
{

	public Step() 
	{
		super("Speedmine", Keyboard.KEY_B, 0);
		setCategory(ModuleCategory.Player);
	}

	@Override
	public void onEnable() 
	{
		EventManager.register(this);
	}

	@Override
	public void onDisable() 
	{
		EventManager.unregister(this);
		Wrapper.getPlayer().stepHeight = 0.5F;
	}
	
	@EventTarget
	  private void onUpdate(EventUpdate event)
	  {
	    if (Wrapper.getPlayer().isCollidedHorizontally) {
	      Wrapper.getPlayer().boundingBox.minY += 0.1D;
	      new Thread(new Runnable()
	      {
	        public void run()
	        {
	          try {
	            Thread.sleep(25L);
	          } catch (InterruptedException e) {
	            e.printStackTrace();
	          }
	          Wrapper.getPlayer().stepHeight = 1.1F;
	        }
	      }).start();
	    } else {
	      Wrapper.getPlayer().stepHeight = 0.5F;
	    }
	  }
}
