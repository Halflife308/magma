package me.jamie.magma.mod.movement;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;

import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;

public class Sneak extends Module
{

	public Sneak() 
	{
		super("Sneak", Keyboard.KEY_Z, 0xFF049B69);
		setVisible(true);
		setCategory(ModuleCategory.Player);
	}

	@Override
	public void onEnable() 
	{
		EventManager.register(this);
	}

	@Override
	public void onDisable() 
	{
		EventManager.unregister(this);
	}
	
}
