package me.jamie.magma.mod.movement;

import me.jamie.magma.events.tick.EventUpdate;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Wrapper;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

public class Sprint extends Module
{

	public Sprint() 
	{
		super("Sprint", Keyboard.KEY_G, 0x66FF00);
		setVisible(true);
		setCategory(ModuleCategory.Player);
	}

	@Override
	public void onEnable() 
	{
		EventManager.register(this);
	}

	@Override
	public void onDisable() 
	{
		EventManager.unregister(this);
		Wrapper.getPlayer().setSprinting(false);
	}
	
	@EventTarget(Priority.LOW)
	private void onUpdate(final EventUpdate event)
	{
		if(Wrapper.getPlayer().movementInput.moveForward != 0.0F && Wrapper.getPlayer().getFoodStats().getFoodLevel() > 6.0F && !Wrapper.getPlayer().isSneaking() && !Wrapper.getPlayer().isInWater()) {
			Wrapper.getPlayer().setSprinting(true);
		}
	}
}
