package me.jamie.magma.mod.movement;

import me.jamie.magma.events.packet.EventPacketSend;
import me.jamie.magma.events.tick.EventUpdate;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Wrapper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.entity.Entity;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.BlockPos;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;
import com.darkmagician6.eventapi.types.Priority;

public class Speed extends Module {
	private byte vel;

	public Speed() {
		super("Speed", Keyboard.KEY_G, -6894535);
		setVisible(true);
		setCategory(ModuleCategory.Player);
	}

	@Override
	public void onEnable() {
		EventManager.register(this);
	}

	@Override
	public void onDisable() {
		EventManager.unregister(this);
		Wrapper.getMinecraft().timer.timerSpeed = 1.0F;
	}

	@EventTarget(Priority.HIGH)
	protected synchronized void onPacketSend(EventPacketSend event) {
		if ((event.getPacket() instanceof C03PacketPlayer)) {
			C03PacketPlayer pp = (C03PacketPlayer) event.getPacket();
			if ((pp.func_149465_i())
					&& (Wrapper.getPlayer().movementInput.moveForward != 0.0F)) {
				this.vel = ((byte) (this.vel + 1));
				if (this.vel == 5) {
					offsetPlayer(pp.getPositionX(), pp.getPositionY() + 0.01D,
							pp.getPositionZ(), false);
				} else if (this.vel >= 10) {
					this.vel = 0;
					event.setCancelled(true);
				}
			} else {
				this.vel = 0;
			}
		}
	}

	@EventTarget
	protected synchronized void onUpdate(EventUpdate event) {
		BlockPos bp = new BlockPos(Wrapper.getPlayer().posX,
				Wrapper.getPlayer().posY, Wrapper.getPlayer().posZ);
		Block b = Wrapper.getWorld().getBlockState(bp).getBlock();

		if ((!Wrapper.getPlayer().isCollidedHorizontally)
				&& (Wrapper.getPlayer().movementInput.moveForward != 0.0F)) {
			this.vel = ((byte) (this.vel + 1));
			if ((this.vel >= 5) && (!isCollided(Wrapper.getPlayer(), b)))
				Wrapper.getMinecraft().timer.timerSpeed = 1.8F;
			else
				Wrapper.getMinecraft().timer.timerSpeed = 1.0F;
		} else {
			Wrapper.getMinecraft().timer.timerSpeed = 1.0F;
		}
	}

	private boolean isCollided(Entity e, Block b) {
		if ((e.isCollidedHorizontally) || (e.isInWater())) {
			return true;
		}
		BlockPos var2 = new BlockPos(e.posX, e.posY - 2.0D, e.posZ);
		if ((b.getBlockState().getBlock() instanceof BlockLiquid)) {
			return true;
		}
		return false;
	}

	private final void offsetPlayer(double x, double y, double z,
			boolean onGround) {
		Wrapper.getPlayer().sendQueue
				.addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(x,
						y, z, onGround));
	}
}
