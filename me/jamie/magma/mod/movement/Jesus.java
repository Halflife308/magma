package me.jamie.magma.mod.movement;

import me.jamie.magma.events.block.EventBoundingBox;
import me.jamie.magma.events.packet.EventPacketSend;
import me.jamie.magma.mod.Module;
import me.jamie.magma.mod.ModuleCategory;
import me.jamie.magma.util.Timer;
import me.jamie.magma.util.Wrapper;
import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.network.Packet;
import net.minecraft.network.play.client.C03PacketPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.BlockPos;
import net.minecraft.util.MathHelper;

import org.lwjgl.input.Keyboard;

import com.darkmagician6.eventapi.EventManager;
import com.darkmagician6.eventapi.EventTarget;

public class Jesus extends Module {
	
	private Timer timer = new Timer();
	
	public Jesus() {
		super("Jesus", Keyboard.KEY_J, 0xFF89BAD3);
		setVisible(true);
		setCategory(ModuleCategory.Player);
	}

	@Override
	public void onEnable() {
		EventManager.register(this);
	}

	@Override
	public void onDisable() {
		EventManager.unregister(this);
	}

	@EventTarget
	private void onBoundingBox(final EventBoundingBox event) {
		if (!Wrapper.getPlayer().isSneaking()
				&& Wrapper.getPlayer().fallDistance < 3.0F) {
			event.setAABB(AxisAlignedBB.fromBounds(event.getX(), event.getY(),
					event.getZ(), event.getX() + 1, event.getY() + 1,
					event.getZ() + 1));
		}
	}
	
	@EventTarget
	private void onPacketSend(final EventPacketSend event) 
	{
		final Packet packet = event.getPacket();
		if(packet instanceof C03PacketPlayer) {
			final C03PacketPlayer player = (C03PacketPlayer)event.getPacket();
			if(isOnLiquid() && (timer.check(180L))) {
				player.y = (player.getPositionY() - 0.01D);
				timer.reset();
			}
			
			if(isInLiquid()) {
				 Wrapper.getPlayer().motionY = 0.08D;
			}
		}
	}

	private boolean isOnLiquid() 
	{
		int var1 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.minX + 0.001D);
		int var2 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.minY + 0.001D);
		int var3 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.minZ + 0.001D);
		int var4 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.maxX - 0.001D);
		int var5 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.maxY - 0.001D);
		int var6 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.maxZ - 0.001D);
		for (int var7 = var1; var7 <= var4; ++var7) {
			for (int var8 = var2; var8 <= var5; ++var8) {
				for (int var9 = var3; var9 <= var6; ++var9) {
					BlockPos pos = new BlockPos(var7, var8 - 2, var9);
					Block block = Wrapper.getWorld().getBlockState(pos)
							.getBlock();
					if (block instanceof BlockLiquid) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private boolean isInLiquid() 
	{
		int var1 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.minX + 0.001D);
		int var2 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.minY + 0.001D);
		int var3 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.minZ + 0.001D);
		int var4 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.maxX - 0.001D);
		int var5 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.maxY - 0.001D);
		int var6 = MathHelper
				.floor_double(Wrapper.getPlayer().boundingBox.maxZ - 0.001D);
		for (int var7 = var1; var7 <= var4; ++var7) {
			for (int var8 = var2; var8 <= var5; ++var8) {
				for (int var9 = var3; var9 <= var6; ++var9) {
					BlockPos pos = new BlockPos(var7, var8, var9);
					Block block = Wrapper.getWorld().getBlockState(pos)
							.getBlock();
					if (block instanceof BlockLiquid) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
