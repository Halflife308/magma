package me.jamie.magma.mod;

public enum ModuleCategory 
{
	Player,
	World,
	Combat,
	Render,
	Other,
}
