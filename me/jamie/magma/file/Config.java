package me.jamie.magma.file;

import java.io.File;
import java.io.IOException;

import me.jamie.magma.util.Wrapper;

public abstract class Config implements IConfig
{
	protected String name;
	protected File dir, config;
	private boolean created;
	
	public Config(String name)
	{
		this.name = name;
		dir = new File(Wrapper.getMinecraft().mcDataDir, "Magma");
		if(!dir.exists()) {
			dir.mkdir();
		}
		try {
			config = new File(dir, getName());
			if(!config.exists()) {
				created = config.createNewFile();
				if(created);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getName() {
		return name;
	}
}
