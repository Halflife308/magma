package me.jamie.magma.file;

import java.io.IOException;

public interface IConfig
{
	public abstract void loadFile() throws IOException;
	
	public abstract void saveFile() throws IOException;
}
