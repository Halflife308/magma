package me.jamie.magma.file;

import java.io.IOException;

import me.jamie.magma.file.files.Friends;
import me.jamie.magma.file.files.Pinned;
import me.jamie.magma.file.files.Positions;

public class FileManager 
{
	private Config friends = new Friends();
	private Config pinned = new Pinned();
	private Config positions = new Positions();
	
	public final void loadFiles() throws IOException
	{
		friends.loadFile();
		pinned.loadFile();
		positions.loadFile();
	}
	
	public Config getFriends() {
		return friends;
	}
	
	public Config getPinned() {
		return pinned;
	}
	
	public Config getPositions() {
		return positions;
	}
}
