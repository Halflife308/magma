package me.jamie.magma.file.files;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import me.jamie.magma.Magma;
import me.jamie.magma.file.Config;
import me.jamie.magma.friend.Friend;

import org.darkstorm.minecraft.gui.component.Frame;

public class Positions extends Config
{

	public Positions() 
	{
		super("positions.txt");
	}

	@Override
	public void loadFile() throws IOException 
	{
		BufferedReader br = new BufferedReader(new FileReader(config));
		String s;
		while((s = br.readLine()) != null)
		{
			String[] args = s.split(":");
			for(Frame frame : Magma.getMagma().getGuiManager().getFrames()) {
				if(frame.getTitle().equalsIgnoreCase(args[0])) {
					frame.setX(Integer.parseInt(args[1]));
					frame.setY(Integer.parseInt(args[2]));
				}
			}
		}
		br.close();
	}

	@Override
	public void saveFile() throws IOException 
	{
		PrintWriter pw = new PrintWriter(config);
		for(Frame f : Magma.getMagma().getGuiManager().getFrames())
		{
			pw.println(f.getTitle() + ":" + f.getX() + ":" + f.getY());
		}
		pw.close();
	}
	
}
