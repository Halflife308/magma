package me.jamie.magma.file.files;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import me.jamie.magma.Magma;
import me.jamie.magma.file.Config;
import me.jamie.magma.friend.Friend;

public class Friends extends Config
{

	public Friends() 
	{
		super("friends.txt");
	}

	@Override
	public void loadFile() throws IOException 
	{
		BufferedReader br = new BufferedReader(new FileReader(config));
		String s;
		while((s = br.readLine()) != null)
		{
			String[] args = s.split(":");
			Magma.getMagma().getFriendManager().addFriend(new Friend(args[0], args[1]));
		}
		br.close();
	}

	@Override
	public void saveFile() throws IOException 
	{
		PrintWriter pw = new PrintWriter(config);
		for(Friend f : Magma.getMagma().getFriendManager().getFriends())
		{
			pw.println(f.getName() + ":" + f.getAlias());
		}
		pw.close();
	}
	
}
