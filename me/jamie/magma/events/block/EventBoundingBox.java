package me.jamie.magma.events.block;

import net.minecraft.util.AxisAlignedBB;

import com.darkmagician6.eventapi.events.Event;

public class EventBoundingBox implements Event
{
	private AxisAlignedBB aabb;
	private final int x, y, z;
	
	public EventBoundingBox(AxisAlignedBB aabb, int x, int y, int z)
	{
		this.aabb = aabb;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public final AxisAlignedBB getAABB() {
		return aabb;
	}
	
	public final void setAABB(AxisAlignedBB aabb) {
		this.aabb = aabb;
	}
	
	public final int getX() {
		return x;
	}
	
	public final int getY() {
		return y;
	}
	
	public final int getZ() {
		return z;
	}
}
