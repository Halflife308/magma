package me.jamie.magma.events.message;

import me.jamie.magma.Magma;
import me.jamie.magma.command.Command;

import com.darkmagician6.eventapi.events.callables.EventCancellable;

public class EventSendChatMessage extends EventCancellable
{
	protected String message;
	
	public EventSendChatMessage(String message)
	{
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public final void onCommand() 
	{
		if(message.startsWith(Magma.getMagma().getCommandManager().getPrefix())) 
		{
			this.setCancelled(true);
			for(Command c : Magma.getMagma().getCommandManager().getCommands()) {
				String[] args = message.split(" ");
				if(args[0].equalsIgnoreCase(Magma.getMagma().getCommandManager().getPrefix() + c.getName())) {
					try {
						c.onCommand(args);
					} catch(Exception e) {
						e.printStackTrace();
						Magma.getMagma().getUtils().addChatMessage("Invalid usage. Try: " + c.getName() + " " + c.getArguments());
					}
				} else {
					Magma.getMagma().getUtils().addChatMessage("Unknown command. Try: " + Magma.getMagma().getCommandManager().getPrefix() + "help");
				}
			}
		}
	}
}
