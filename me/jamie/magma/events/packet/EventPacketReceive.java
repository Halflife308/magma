package me.jamie.magma.events.packet;

import net.minecraft.network.Packet;

import com.darkmagician6.eventapi.events.callables.EventCancellable;

public class EventPacketReceive extends EventCancellable
{
	private Packet p;
	
	public EventPacketReceive(Packet p)
	{
		this.p = p;
	}

	public Packet getPacket() 
	{
		return p;
	}
}
