package me.jamie.magma.events.packet;

import net.minecraft.network.Packet;

import com.darkmagician6.eventapi.events.callables.EventCancellable;

public class EventPacketSend extends EventCancellable
{
	private Packet p;
	
	public EventPacketSend(Packet p)
	{
		this.p = p;
	}
	
	public Packet getPacket() {
		return p;
	}
}
