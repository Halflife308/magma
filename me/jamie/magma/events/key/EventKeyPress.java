package me.jamie.magma.events.key;

import me.jamie.magma.Magma;
import me.jamie.magma.mod.Module;

import com.darkmagician6.eventapi.events.Event;

public class EventKeyPress implements Event
{
	private int key;
	
	public EventKeyPress(int key)
	{
		this.key = key;
	}
	
	public int getKey() {
		return key;
	}
	
	public final void onKeyToggle() {
		for(Module m : Magma.getMagma().getModuleManager().getModules()) {
			if(m.getBind() == key) {
				m.onToggle();
			}
		}
	}
}
